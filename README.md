# YDLoop
Youtube Downloader Shell Loop in C++

I ran into the issue where I wanted to download multiple videos but didn't want to have to backspace the old video url in the console...

This could easily be rewritten in bash and avoid the entire G++ compile step 

# Dependencies
  Youtube-dl

# Building the code

g++ main.cpp -o whateveryouwanttonameit
